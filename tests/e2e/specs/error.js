// https://docs.cypress.io/api/introduction/api.html
const TODAY = new Date();

const TODAY_DATE = `${TODAY.getFullYear() - 1}/${
  TODAY.getMonth() + 1 < 10
    ? '0' + (TODAY.getMonth() + 1)
    : TODAY.getMonth() + 1
}/${TODAY.getDate() < 10 ? '0' + TODAY.getDate() : TODAY.getDate()}`;

const TOMORROW_DATE = `${TODAY.getFullYear() - 1}/${
  TODAY.getMonth() + 1 < 10
    ? '0' + (TODAY.getMonth() + 1)
    : TODAY.getMonth() + 1
}/${TODAY.getDate() < 10 ? '0' + (TODAY.getDate() + 1) : TODAY.getDate() + 1}`;

describe('To Index', () => {
  it('Visits the app root url', () => {
    cy.visit('/');
    cy.contains('h1', 'DOGE PAGE');
  });
});

describe('Search Flight', () => {
  it('Visits the app root url', () => {
    cy.visit('/#/booking');

    cy.get('.cabin').should('be.visible');

    cy.get('[data-test="go-airport-selector"]').click();
    cy.get('[data-test="go-airport-selector"] .multiselect__option')
      .contains('桃園國際機場')
      .click();

    cy.get('[data-test="back-airport-selector"]').click();
    cy.get('[data-test="back-airport-selector"] .multiselect__option')
      .contains('香港國際機場')
      .click();

    cy.get('[data-test="go-calendar"]').click();
    cy.get(`[data-test="go-calendar"] [data-date="${TODAY_DATE}"]`).click();

    cy.get(
      `[data-test="back-calendar"] [data-date="${TOMORROW_DATE}"]`,
    ).click();

    cy.get('[data-test="adt-pax-stepper"] .increase').click();
    cy.get('[data-test="inf-pax-stepper"] .increase').click();
    cy.get('[data-test="big-chd-pax-stepper"] .increase').click();
    cy.get('[data-test="small-chd-pax-stepper"] .increase').click();

    cy.contains('button', '搜尋航班').click();
  });
});
