# starlux-vue-component

Starlux Website 組件開發

## Requirement

node.js v8.11.x 以上

## Vscode Linter Extension

- ESLint
- Prettier - Code formatter
- stylelint

## Getting Started

專案安裝步驟

```shell
# 安裝
$ npm install # Or yarn install

# development mode，在 localhost:3000 上跑
$ npm run serve

# aja mode，回傳 fakejson
$ npm run serve-aja

# production mode，在 /dist 產生打包專案
$ npm run production

# 運行測試並在根目錄產生 .jest-test-results.json 測試結果
# 如果 storybook 要顯示測試結果要現執行這行!!
$ npm run test:generate-output

# storybook development mode ，在 localhost:6006 上跑
$ npm run storybook

# story book production mode，在 /dist/storybook 產生打包專案
$ npm run build-storybook
```

開發用指令

```shell
# 用 plop 產生 component
$ npm run plop
```

## Docker Container Getting Started

專案安裝步驟

```
#docker build image ( --no-cache  ) 
#GITUSER GITTOKEN GITPROJECT
#docker build --tag  [[YOUR APP]]  --build-arg GITUSER= [[USERNAME]]  --build-arg GITTOKEN= [[TOKEN]]  --build-arg GITPROJECT=official-website/starlux-vue-component .

docker build --tag docker.starlux-airlines.com/official-website/starlux-vue-component --build-arg GITUSER=1802855 --build-arg GITTOKEN=jD_CVtJzz-Hd5RxHaztT --build-arg GITPROJECT=official-website/starlux-vue-component .

#stop && remove docker container (if already use)
docker stop starlux-vue-component && docker rm starlux-vue-component

#run docker image
docker run -d --name starlux-vue-component -p 80:8081 docker.starlux-airlines.com/official-website/starlux-vue-component

```


## Project Structure

重要目錄與檔案

```shell
├─internal
├─public         #靜態檔
├─src
│  ├─api
│  ├─assets      #會編譯的圖檔
│  ├─components  #組件
│  ├─lang        #i18n
│  ├─style       #SCSS
│  ├─views       #頁面
│  ├─App.vue
│  ├─main.js
│  ├─router.js
│  └─store.js
...
```

## Used Packages

敘述使用套件

| 套件名稱 | 簡介 | 敘述 |
| -------- | ---- | ---- |
|          |      |      |
