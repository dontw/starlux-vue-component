import { addDecorator,configure } from '@storybook/vue';
import { withOptions} from '@storybook/addon-options';
import { withNotes} from '@storybook/addon-notes'
import { withTests } from '@storybook/addon-jest'
import results from '../../.jest-test-results.json'

addDecorator(
  withOptions({
    name: 'starlux-vue storybook',
    url: 'https://gitlab.starlux-airlines.com/official-website/starlux-vue-component',
    addonPanelInRight: false,
  })
)

addDecorator(withNotes)

addDecorator(withTests({results}))

require('!!style-loader!css-loader!sass-loader!../../src/style/main.scss');

// automatically import all files ending in *.stories.js
const req = require.context('../../src', true, /.stories.js$/);

function loadStories() {
  req.keys().forEach((filename) => req(filename));
}

configure(loadStories, module);
