const path = require('path');
module.exports = (storybookBaseConfig, configType, defaultConfig) => {
  defaultConfig.resolve = {
    alias: {
      ...defaultConfig.resolve.alias,
      '~': path.resolve(__dirname, '../../src'),
      '@': path.resolve(__dirname, '../../src'),
    },
  };
  defaultConfig.module.rules.push({
    test: /\.scss$/,
    use: [
      'vue-style-loader',
      'css-loader',
      'sass-loader',
      {
        loader: 'sass-resources-loader',
        options: {
          resources: [path.resolve(__dirname, '../../src/style/main.scss')],
        },
      },
    ],
  });
  return defaultConfig;
};
