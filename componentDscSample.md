# StarDropDown 下拉選單

## Code Example

### Basic Usage

```html
<StarDropdown options="options" v-model="value" :maxHeight="500" title="稱謂" />
```

### Some Special Usage

...

## Props

| name        | description    | type   | default | sample\*                                       |  必須  |
| ----------- | -------------- | ------ | ------- | ---------------------------------------------- | ------ |
| options     | 選項內容       | Array  | 無      | `[ {label:'選項名稱', value:'選項數值'},... ]` | ✅     |
| maxHeight   | 高度           | Number | 300     |                                                |        |
| title       |  標題標籤內容  | String | 無      |                                                | ✅     |
| placeholder |  預設說明字    | String | 無      |                                                | ✅     |

## Events

...

## Slots

...






# Calendar 日曆

## Code Example
### Basic Usage

```html
單欄
<calendar v-model="value" :format="format" :placeholder="placeholder" :pane="2"></calendar>

二欄
<calendar v-model="value1" :format="format" :placeholder="placeholder" :pane="2" :range-bus="getBus" :range-status="1"></calendar>
<calendar v-model="value2" :format="format" :placeholder="placeholder" :pane="2" :range-bus="getBus" :range-status="2"></calendar>
```

### Some Special Usage

...

## Props

| name         | description                                   | type   | default     | sample\*                    |  必須  |
| ------------ | ----------------------------------------------| ------ | ------------| ----------------------------| ------ |
| value        | 日期value                                      | String | 無          |                             | ✅     |
| format       | 日期格式                                        | String | yyyy-MM-dd | ` yyyy/MM/dd `               |        |
| inputClasses | 自訂 class                                     | String | 無          |                             |        |
| lang         | 多國語, 待完整                                  | String | navigator.language|                       |        |
| placeholder  | placeholder                                   | String | 無          |                              |        |
| hasInput     | 有沒有 input 欄位                               | Boolean | true      |                              |        |
| pane         | 月份 panel 數量                                | Number  | 1          |                              |        |
| borderWidth  | 月份 panel border                             | Number  | 2          |                              |        |
| onDayClick   | 只有當 hasInput 設 false 時有用                 | Function| 無         |                              |        |
| changePane   | 切換 pane 顯示的內容, (year, month, pane)       | Function| 無         |                              |        |
| rangeBus     | 前後 datepicker range 溝通用, 會回傳 new Vue()  | Function | 無        |                               |        |
| rangeStatus  | range 關連用, 目前只能用到 [0, 1, 2], 1 會 和 2 溝通 | Number | 0       |                               |        |
| onDrawDate   | 更新日曆能否點選 allowSelect                    | Function | 無        |                               |        |
| showDateOnly | 是否只顯示日曆                                  | Boolean | false     |                               |        |
| transfer     | 傳送 popup 視窗到 document.body                | Boolean  | false     |                               |        |
| elementId    | 給 label for="" 使用的 id                      | String   | 無        |                               |        |


## Events

### drawdate - drawdate 事件, 參考 onDrawDate

## Slots

...

## ToDo
### 當有 rangeStatus 時, 要能自動展開下一個 datepicker
### 當用 keyboard 時, focusOut 時要關閉 datepicker
### 多國語





# StarPriceSwiper 票價

## Code Example
### Basic Usage

```html
單欄



```

### Some Special Usage
外層使用 https://github.com/surmon-china/vue-awesome-swiper


...

## Props

| name         | description                                   | type   | default     | sample\*                    |  必須  |
| ------------ | ----------------------------------------------| ------ | ------------| ----------------------------| ------ |
| 待補


## Events



## Slots

...

## ToDo
### 確認 API 串接
### 確認 假資料與 Vuex 使用範圍


# CabinClassSelector
## Basic Usage
<cabin-class-selector v-show="item===pickedFlightNo" :cabin="cabin" :flightType="type"></cabin-class-selector>

## Props

| name        | description    | type   | default | sample\*                                       |  必須  |
| ----------- | -------------- | ------ | ------- | ---------------------------------------------- | ------ |
| cabin       | 票價資訊        | Obj     | 無      |                                                | ✅     |
| flightType  | 辨認為Dep或是Arr | String | 無     |                                                  | ✅     |

cabin Obj結構：
cabin:{
  payForPassClass: 9990,
  economyCabinClassList:{
    0:{
      className: '經濟Save',
      price: 8000,
      availableSeat: 0,
      wifi: false,
      luggage: 20,
      milage: 100,
      handluggage: 7,
      seatSelection: false,
      seat:{
        position: '標準',
        info: '12-33 排座位'
      },
      millageCumulative: false,
      allowRefund: 0,
      allowChangePlan: 2888,
      allowChangeFlight: 2888,
      allowMissedRefund: 2888
    },
  },
  commercialCabinClassList:{
    0:{
      className: '商務 Standard',
      price: 19925,
      availableSeat: null,
      wifi: true,
      luggage: 45,
      milage: 130,
      handluggage: 7,
      seatSelection: false,
      seat:{
        position: '標準',
        info: '12-33 排座位'
      },
      millageCumulative: false,
      allowRefund: 0,
      allowChangePlan: 2888,
      allowChangeFlight: 2888,
      allowMissedRefund: 2888
    }
  }
},

## Events
cabinSelect(val, type) {
  if (type == 'dep') {
    this.$store.commit('depSelectCabin', val);
  } else if (type == 'arr') {
    this.$store.commit('arrSelectCabin', val);
  }
},
根據點擊艙等更新相對應的數據到sate上
switchFlightModal(index, cabinClass){
  this.$store.commit({
    type: 'flightModalDepSwitch',
    index: index,
    cabinClass: cabinClass,
    opened: true
  });
}
根據點擊的info顯示對應的modal與內容，index是映照反白的對象

...
---

# StarBabinListModal

## Basic Usage
<Star-cabin-list-modal></Star-cabin-list-modal>

## Event
getCabinData(){
  let cabinClass = this.$store.state.flightModalDepActivation.cabinClass;
  if(cabinClass==='economy'){
    return this.$store.state.cabin.economyCabinClassList;
  }else if(cabinClass==='commercial'){
    return this.$store.state.cabin.commercialCabinClassList;
  }
}

資料是根據this.$store.state.flightModalDepActivation取得


# StarFlightDetailModal

## Basic Usage
<Star-flight-detail-modal></Star-flight-detail-modal>

## Event
getFlightDetailData(){
  console.log(this.$store.state.flightDetailData);
  return this.$store.state.flightDetailData;
}

資料是根據this.$store.state.flightDetailData取得
