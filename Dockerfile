# Base image node js
FROM docker.starlux-airlines.com/core-service/docker-image-nginx-frontend

# Install 
RUN apk update && apk upgrade &&\
    apk add --no-cache \
    make \
    gcc \
    g++ \
    git \
    python \
    py-pip \
    vim \
    nodejs-current-npm

RUN apk add --update nodejs
RUN cd / && npm set prefix /usr  && npm set prefix /usr/share

#run mkdir
RUN mkdir -p /usr/share/nginx/html/project 

# Git Clone Project
# ARG GITUSER GITTOKEN GITPROJECT
ARG GITUSER="1802855"
ARG GITTOKEN="jD_CVtJzz-Hd5RxHaztT"
ARG GITPROJECT="official-website/starlux-vue-component"
RUN git clone https://${GITUSER}:${GITTOKEN}@gitlab.starlux-airlines.com/${GITPROJECT} /usr/share/nginx/html/project
RUN cd /usr/share/nginx/html/project && git pull

#npm install
RUN cd /usr/share/nginx/html/project && npm install
RUN cd /usr/share/nginx/html/project && npm run test:generate-output

#nginx conf
COPY ./docker/nginx.conf /etc/nginx/
COPY ./docker/run.sh /usr/share/nginx/



EXPOSE 80 8080

CMD ["nginx", "-g", "daemon off;"]

