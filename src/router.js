import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Test from './views/Test.vue'
//Booking
import Booking from './views/booking/Index.vue';
import SearchFlight from './views/booking/SearchFlight.vue';
import SelectPrice from './views/booking/SelectPrice.vue';
import PaxInfo from './views/booking/PaxInfo.vue';
import Payment from './views/booking/Payment.vue';
import Completed from './views/booking/Completed.vue';
import Schedule from './views/booking/Schedule';
import CustomizedServices from './views/booking/customizedServices/Index.vue';
import CustomizedServicesList from './views/booking/customizedServices/CustomizedServicesList.vue';
import SelectSeat from './views/booking/customizedServices/SelectSeat.vue';
import SelectMeal from './views/booking/customizedServices/SelectMeal.vue';

//Check in
import CheckIn from './views/checkIn/Index.vue';
import FindBookingFlight from './views/checkIn/FindBookingFlight.vue';
import SelectCheckInFlight from './views/checkIn/SelectCheckInFlight.vue';
import SelectCheckInPax from './views/checkIn/SelectCheckInPax';
import CheckInPaxInfo from './views/checkIn/PaxInfo.vue';
import ConfirmLuggageNotice from './views/checkIn/ConfirmLuggageNotice.vue';
import CheckInCompleted from './views/checkIn/Completed.vue';
import BoardingPass from './views/checkIn/BoardingPass.vue';

Vue.use(Router);

const CUSTOM_ROUTER =  new Router({
  scrollBehavior() {
    return { x: 0, y: 0 };
  },

  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path:'/test',
      name: 'test',
      component: Test
    },
    //BOOKING
    {
      path: '/booking',
      component: Booking,
      children: [
        { path: '', name: 'searchFlight', component: SearchFlight },
        {
          path: 'selectPrice',
          name: 'selectPrice',
          component: SelectPrice,
        },
        {
          path: 'paxInfo',
          name: 'paxInfo',
          component: PaxInfo,
        },
        {
          path: 'customizedServices',
          component: CustomizedServices,
          children: [
            { path: '', name: 'customizedServices', component: CustomizedServicesList },
            {
              path: 'selectSeat',
              name: 'customizedServices-selectSeat',
              component: SelectSeat,
            },
            {
              path: 'selectMeal',
              name: 'customizedServices-selectMeal',
              component: SelectMeal,
            },
          ]
        },
        {
          path: 'payment',
          name: 'payment',
          component: Payment,
        },
        {
          path: 'completed',
          name: 'completed',
          component: Completed,
        },
        {
          path: 'schedule',
          name: 'schedule',
          component: Schedule,
        },
      ]
    },
    //CHECK IN
    {
      path: '/checkIn',
      component: CheckIn,
      children: [
        { path: '', name: 'findBookingFlight', component: FindBookingFlight },
        {
          path: 'selectCheckInPax',
          name: 'selectCheckInPax',
          component: SelectCheckInPax,
        },
        {
          path: 'selectCheckInFlight',
          name: 'selectCheckInFlight',
          component: SelectCheckInFlight,
        },
        {
          path: 'paxInfo/:id',
          name: 'checkIn-paxInfo',
          component: CheckInPaxInfo,
        },
        {
          path: 'confirmLuggageNotice',
          name: 'confirmLuggageNotice',
          component: ConfirmLuggageNotice,
        },
        {
          path: 'completed',
          name: 'checkIn-completed',
          component: CheckInCompleted,
        },
      ],
    },
    {
      path: '/boardingPass',
      name: 'boardingPass',
      component: BoardingPass,
    },
  ],
});

export default CUSTOM_ROUTER;
