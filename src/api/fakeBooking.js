import cabin from '../json/cabin.json'; // get cabin list
import airports from '../json/airports.json';  // get airport list
import nearbyAirport from '../json/nearbyAirport.json'; // get nearbyAorport airportcode
import fareFamily from '../json/fareFamily.json'; // get farefamily 
// import flightForSearch from '../json/flightForSearch.json'; // old version
import flights from '../json/flights.json'; // get flights available
import flightDetails from '../json/flightDetails.json'; // get flights available
import createCart from '../json/createCart.json'; // get cartId
import paxInfo from '../json/paxInfo.json'; // get cartId
import cartInfo from '../json/cartInfo.json'; // get cartId

export default {
  getCabin(lang) {
    return Promise.resolve({ data: cabin });
  },

  getAirport(lang) {
    return Promise.resolve({ data: airports });
  },

  getNearbyAirportCode(data) {
    return Promise.resolve({ data: nearbyAirport });
  },

  getSingleFareFamily(data) {
    return Promise.resolve({data: fareFamily});
  },

  getRoundFareFamily(data) {
    return Promise.resolve({data: fareFamily});
  },

  getSingleFlight(data) {
    return Promise.resolve({data: flights});
  },

  getRoundFlight(data) {
    return Promise.resolve({data: flights});
  },

  getGoFlightDetails(data) {
    return Promise.resolve({data: flightDetails});
  },

  getBackFlightDetails(data) {
    return Promise.resolve({data: flightDetails});
  },

  createCart(data) {
    return Promise.resolve({data: createCart});
  },

  updatePaxInfo(data) {
    return Promise.resolve({data: paxInfo});
  },

  getCartInfo(data) {
    return Promise.resolve({data: cartInfo});
  }

};
