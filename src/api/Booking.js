import Instance from './Instance';

export default {
  getCabin(lang) {
    return Instance.get('/cabin', {
      params: {
        lang: lang,
      },
    });
  },

  getAirport(lang) {
    return Instance.get('/airport', {
      params: { lang: lang },
    });
  },

  getNearbyAirportCode(data) {
    return Instance.get('/airport/nearbyAirportCode', {
      params: {
        lng: data.lng,
        lat: data.lat,
      },
    });
  },

  getSingleFareFamily(data) {
    return Instance.get('/airCalendars', {
      params: {
        goDate: data.goDate,
        depAirport: data.depAirport,
        arrAirport: data.arrAirport,
        cabin: data.cabin,
        adt: data.pax.adt,
        chd: data.pax.chd,
        inf: data.pax.inf,
      },
    });
  },

  getRoundFareFamily(data) {
    return Instance.get('/airCalendars', {
      params: {
        goDate: data.goDate,
        backDate: data.backDate,
        depAirport: data.depAirport,
        arrAirport: data.arrAirport,
        cabin: data.cabin,
        adt: data.pax.adt,
        chd: data.pax.chd,
        inf: data.pax.inf,
      },
    });
  },

  getSingleFlight(data) {
    return Instance.get('/flight', {
      params: {
        goDate: data.goDate,
        depAirport: data.depAirport,
        arrAirport: data.arrAirport,
        cabin: data.cabin,
        adt: data.pax.adt,
        chd: data.pax.chd,
        inf: data.pax.inf,
      },
    });
  },

  getRoundFlight(data) {
    return Instance.get('/flight', {
      params: {
        goDate: data.goDate,
        backDate: data.backDate,
        depAirport: data.depAirport,
        arrAirport: data.arrAirport,
        cabin: data.cabin,
        adt: data.pax.adt,
        chd: data.pax.chd,
        inf: data.pax.inf,
      },
    });
  },

  getFlightDetails(data) {
    return Instance.get('/flight/flightDetails', {
      params: {
        goDate: data.goDate,
        backDate: data.backDate,
        depAirport: data.depAirport,
        arrAirport: data.arrAirport,
        adt: data.pax.adt,
        chd: data.pax.chd,
        inf: data.pax.inf,
        cabin: data.cabin,
        goFlightNo: data.goFlightNo,
        backFlightNo: data.backFlightNo,
        isReturn: data.isReturn,
      },
    });
  },

  getGoFlightDetails(data) {
    return Instance.get('/flight/flightDetails', {
      params: {
        goDate: data.goDate,
        depAirport: data.depAirport,
        arrAirport: data.arrAirport,
        adt: data.pax.adt,
        chd: data.pax.chd,
        inf: data.pax.inf,
        cabin: data.cabin,
        goFlightNo: data.goFlightNo,
        isReturn: data.isReturn,
      },
    });
  },

  getBackFlightDetails(data) {
    return Instance.get('/flight/flightDetails', {
      params: {
        goDate: data.goDate,
        backDate: data.backDate,
        goFareFamilyCode: data.goFareFamilyCode,
        depAirport: data.depAirport,
        arrAirport: data.arrAirport,
        adt: data.pax.adt,
        chd: data.pax.chd,
        inf: data.pax.inf,
        cabin: data.cabin,
        goFlightNo: data.goFlightNo,
        isReturn: data.isReturn,
      },
    });
  },

  createCart(data) {
    return Instance.post('/cart', {
      airOfferId: data.airOfferId,
      goDate: data.goDate,
      backDate: data.backDate,
      goFlight: data.goFlightNo,
      backFlight: data.backFlightNo,
      cabin: data.cabin,
      paxCount: data.pax,
    });
  },

  updatePaxInfo(data) {
    return Instance.post(`/cart/${data.cartId}`, {
      airOfferId: data.airOfferId,
      pax: data.pax,
      contact: data.contact,
    });
  },

  getCartInfo(cartId) {
    return Instance.get(`/cart/${cartId}`);
  },

};
