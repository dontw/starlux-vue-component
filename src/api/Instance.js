import axios from 'axios';
import store from '../store/index';

const Instance = axios.create({
  timeout: 1000 * 30,
  headers: { 'Content-type': 'application/json' },
});

if (process.env.NODE_ENV != 'development') {
  Instance.defaults.baseURL = 'https://api.starlux-airlines.com/api';
}

Instance.interceptors.request.use(
  (config) => {
    store.commit('loading', true);
    return config;
  },
  (err) => {
    return Promise.reject(err);
  },
);

Instance.interceptors.response.use(
  (res) => {
    store.commit('loading', false);
    return res;
  },
  (err) => {
    return Promise.reject(err);
  },
);

export default Instance;
