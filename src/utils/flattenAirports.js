/**
 * 攤平機場列表
 * @param {Array} airports 機場列表
 * @return {Array} 可以給 multi-select 的機場列表
 */
export default function(airports) {
  let newArr = [];
  airports.forEach((el) => {
    const newVal = el.countries.reduce((accu, curr) => {
      curr.cities.map((city) => {
        city.airports.map((airport) => {
          curr.cityLocName = city.locName;
          curr.cityEngName = city.engName;
          curr.cityCode = city.code;
          curr.airportLocName = airport.locName;
          curr.airportEngName = airport.engName;
          curr.airportCode = airport.code;
          delete curr.cities;
          accu.push({ ...curr });
        });
      });
      return accu;
    }, []);
    el.countries = [...newVal];
    newArr.push(el);
  });
  return newArr;
}
