import Vue from 'vue';
import VueI18n from 'vue-i18n';
import en from './lang/en.json';
import ja from './lang/ja.json';
import zh_TW from './lang/zh-TW.json';
import dateTimeFormats from './lang/dateTimeFormats.json';

Vue.use(VueI18n);

const lang = {
  en: en,
  ja: ja,
  'zh-TW': zh_TW,
};

//set lang attribute to html tag
document.documentElement.setAttribute('lang', 'zh-TW');

const i18n = new VueI18n({
  locale: 'zh-TW',
  fallbackLocale: 'zh-TW',
  messages: lang,
  dateTimeFormats,
});

export default i18n;
