export default {
  SET_NAME(state, data) {
    state.name = data;
  },

  SET_PAX_DATA_LIST(state, data) {
    state.paxDataList = data;
  },
};
