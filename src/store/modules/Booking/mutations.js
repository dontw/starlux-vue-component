import cloneDeep from 'lodash/cloneDeep'
export default {
  RESET_BOOKING_DATA(state) {
    state.paxList = [];
    state.contactList = [];
    state.goFlight = {isSelected: false};
    state.backFlight = {isSelected: false};
    state.cartId = null;
    state.goFlightDetails = [];
    state.backFlightDetails = [];
  },

  SET_CABINS(state, data) {
    state.cabins = data;
  },

  SET_AIRPORTS(state, data) {
    state.airports = data;
  },

  SET_NEARBY_AIRPORT_CODE(state, data) {
    state.nearbyAirportCode = data;
  },

  SET_FARE_FAMILY(state, data) {
    state.fareFamily = data;
  },

  SET_CURRENCY(state, data) {
    state.currency = data;
  },

  SET_RAW_SEARCH_INFO(state, data) {
    state.rawSearchInfo = cloneDeep(data);
  },

  SET_DEP_AIRPORT(state, data) {
    state.rawSearchInfo.depAirport = data;
  },

  SET_ARR_AIRPORT(state, data) {
    state.rawSearchInfo.arrAirport = data;
  },

  SET_FLIGHTS(state, data) {
    state.flights = data;
  },

  SET_GO_FLIGHT(state, data) {
    state.goFlight = data;
  },

  SET_BACK_FLIGHT(state, data) {
    state.backFlight = data;
  },

  SET_PAX_LIST(state, data) {
    state.paxList = cloneDeep(data);
  },

  SET_CONTACT_LIST(state, data) {
    state.contactList = cloneDeep(data);
  },
  
  SET_GO_FLIGHT_DETAILS(state, data) {
    state.goFlightDetails = data;
  },

  SET_BACK_FLIGHT_DETAILS(state, data) {
    state.backFlightDetails = data;
  },

  RESET_FLIGHT(state) {
    state.goFlight = {isSelected: false};
    state.backFlight = {isSelected: false};
    state.cartId = null;
    state.goFlightDetails = [];
    state.backFlightDetails = [];
  },

  SET_CART_ID(state, data) {
    state.cartId = data;
  },

  SET_AIR_OFFER_ID(state, data) {
    state.airOfferId = data; 
  },

  SET_CART_INFO(state, data) {
    state.cartInfo = data;
  }
};
