
import actions from './actions.js';
import mutations from './mutations.js';
import getters from './getters.js'
export default {
  namespaced: true,
  state: {
    cabins: null, //Array 倉等列表
    airports: [], //Array 機場列表
    nearbyAirportCode: null, //String 鄰近機場代號
    rawSearchInfo: {
      //Object 原始本次搜尋資料
      goDate: '', //String YYYY/MM/DD
      backDate: '', //String YYYY/MM/DD
      depAirport: null, //object
      arrAirport: null, //object
      cabin: 'Y',
      pax: {
        adt: 1,
        bigChd: 0,
        smallChd: 0,
        inf: 0,
      },
      isRoundTrip: true,
    },
    fareFamily: [], // Array 航班價格列表
    currency: null, // String 目前幣別
    flights: [], // Array 去回程航班列表
    goFlight: {
      isSelected: false,
    }, // Object 選擇去程之航班
    backFlight: {
      isSelected: false,
    }, // Object 選擇回程之航班
    paxList: [], // Array 旅客資訊列表
    contactList: [], // Array 聯絡人資訊列表 但目前只會塞一個物件幹嘛啦
    goFlightDetails: [],
    backFlightDetails: [],
    cartId: null,
    airOfferId: null,
    cartInfo: {
      pax: [],
      unitPrice: [],
      contact: [],
    },
  },
  getters,
  mutations,
  actions,
};
