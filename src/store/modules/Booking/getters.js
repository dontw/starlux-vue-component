import flattenAirports from '@/utils/flattenAirports.js';
import cloneDeep from 'lodash/cloneDeep';

export default  {
  //取得可以被 multi-select 使用的 airports array options
  GET_AIRPORT_OPTIONS(state) {
    return flattenAirports(cloneDeep(state.airports));
  },

  GET_NEARBY_AIRPORT(state, getters) {
    let arr = [];
    getters.GET_AIRPORT_OPTIONS.forEach((option) => {
      let target = option.countries.filter((country) => {
        return country.airportCode === state.nearbyAirportCode;
      });
      if (target.length >= 1) {
        arr = target;
      }
    });
    return arr[0];
  },

  GET_RAW_SEARCH_INFO(state){
    return cloneDeep(state.rawSearchInfo)
  },

  GET_SEARCH_INFO(state) {
    let {
      goDate,
      backDate,
      isRoundTrip,
      depAirport,
      arrAirport,
      cabin,
      pax,
    } = state.rawSearchInfo;

    if (depAirport && arrAirport) {
      return {
        goDate,
        backDate: isRoundTrip ? backDate : null,
        depAirport: depAirport.airportCode,
        arrAirport: arrAirport.airportCode,
        cabin,
        pax: {
          adt: pax.adt,
          chd: pax.bigChd + pax.smallChd,
          inf: pax.inf,
        },
        isRoundTrip,
      };
    }
  },

  GET_GO_FARE_FAMILY(state) {
    return state.fareFamily.filter((item) => item.isReturn === false);
  },

  GET_BACK_FARE_FAMILY(state) {
    return state.fareFamily.filter((item) => item.isReturn === true);
  },

  GET_ALL_PAX_NUM(state) {
    let { adt, bigChd, smallChd, inf } = state.rawSearchInfo.pax;
    return adt + bigChd + smallChd + inf;
  },

  GET_CURR_CABIN_NAME(state) {
    if (state.cabins) {
      return state.cabins.filter(
        (item) => item.code === state.rawSearchInfo.cabin,
      )[0].name;
    }
    return false;
  },

  GET_GO_FLIGHTS(state) {
    return state.flights.filter((item) => item.isReturn === false);
  },

  GET_BACK_FLIGHTS(state) {
    return state.flights.filter((item) => item.isReturn === true);
  },

  GET_PAX_LIST(state) {
    const CART_INFO_PAX = state.cartInfo.pax;
    const CART_INFO_PRICE = state.cartInfo.unitPrice;
    return CART_INFO_PAX.map((pax) => {
      const PRICES = CART_INFO_PRICE.find((el) => el.travelerId === pax.id);
      pax.prices = PRICES.prices;
      pax.totalPrice = PRICES.totalPrice;
      return pax;
    });
  },
}
