//! 記得切回真的API!!!!!!!!!!!!!
import BookingApi from '@/api/Booking.js';

export default {
  GET_CABINS({ commit, state }, lang) {
    return BookingApi.getCabin(lang)
      .then((res) => {
        if (res.data.status) {
          commit('SET_CABINS', res.data.result.data);
        } else {
          throw new Error('Can not find any cabin data');
        }
      })
      .catch((err) => {
        throw new Error(err);
      })
      .finally(() => {
        console.log('state.cabins', state.cabins);
      });
  },

  GET_AIRPORTS({ commit, state }, lang) {
    return BookingApi.getAirport(lang)
      .then((res) => {
        commit('SET_AIRPORTS', res.data.result.data);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        console.log('state.airports', state.airports);
      });
  },

  GET_NEARBY_AIRPORT_CODE({ commit, state }, params) {
    return BookingApi.getNearbyAirportCode(params)
      .then((res) => {
        commit('SET_NEARBY_AIRPORT_CODE', res.data.result.data[0].airportCode);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        console.log('state.nearbyAirportCode', state.nearbyAirportCode);
      });
  },

  GET_SINGLE_FARE_FAMILY({ commit, state }, params) {
    commit('RESET_BOOKING_DATA');
    return BookingApi.getSingleFareFamily(params)
      .then((res) => {
        commit('SET_FARE_FAMILY', res.data.result.data);
        commit('SET_CURRENCY', res.data.result.meta.currency);
      })
      .catch((err) => {
        console.log('GET_SINGLE_FARE_FAMILY_ERR', err);
      })
      .finally(() => {
        console.log('state.fareFamily', state.fareFamily);
      });
  },

  GET_ROUND_FARE_FAMILY({ commit, state }, params) {
    commit('RESET_BOOKING_DATA');
    return BookingApi.getRoundFareFamily(params)
      .then((res) => {
        commit('SET_FARE_FAMILY', res.data.result.data);
        commit('SET_CURRENCY', res.data.result.meta.currency);
      })
      .catch((err) => {
        console.log('GET_ROUND_FAREFAMILY');
      })
      .finally(() => {
        console.log('state.farefamily', state.fareFamily);
      });
  },

  GET_SINGLE_FLIGHTS({ commit, state }, params) {
    return BookingApi.getSingleFlight(params)
      .then((res) => {
        console.log('get single flights', res);
        commit('SET_FLIGHTS', res.data.result.data);
      })
      .catch((err) => {
        console.log('GET_FLIGHTS_ERR', err);
      })
      .finally(() => {
        console.log('state.flights', state.flights);
      });
  },

  GET_ROUND_FLIGHTS({ commit, state }, params) {
    return BookingApi.getRoundFlight(params)
      .then((res) => {
        console.log('get round flights', res);
        commit('SET_FLIGHTS', res.data.result.data);
      })
      .catch((err) => {
        console.log('GET_FLIGHTS_ERR', err);
      })
      .finally(() => {
        console.log('state.flights', state.flights);
      });
  },

  GET_GOFLIGHT_DETAILS({ commit }, params) {
    return BookingApi.getGoFlightDetails(params)
      .then((res) => {
        console.log('GO_FLIGHT_DETAILS', res);
        commit('SET_GO_FLIGHT_DETAILS', res.data.result.data);
      })
      .catch((err) => {
        console.log('GET_GOFLIGHT_DETAILS', err);
      });
  },

  GET_BACKFLIGHT_DETAILS({ commit }, params) {
    return BookingApi.getBackFlightDetails(params)
      .then((res) => {
        console.log('BACK_FLIGHT_DETAILS', res);
        commit('SET_BACK_FLIGHT_DETAILS', res.data.result.data);
      })
      .catch((err) => {
        console.log('GET_BACKFLIGHT_DETAILS', err);
      });
  },

  CREATE_CART_ID({ commit }, params) {
    return BookingApi.createCart(params)
      .then((res) => {
        console.log('createCart', res);
        commit('SET_CART_ID', res.data.result.data[0].cartId);
      })
      .catch((err) => {
        console.log('CREATE_CART_ID', err);
      });
  },

  UPDATE_PAX_INFO({ commit }, params) {
    return BookingApi.updatePaxInfo(params)
      .then((res) => console.log('update pax info', res))
      .catch((err) => console.log('update pax info error', err));
  },

  GET_CART_INFO({ commit }, cartId) {
    return BookingApi.getCartInfo(cartId)
      .then((res) => {
        console.log('GET_CART_INFO', res);
        commit('SET_CART_INFO', res.data.result.data[0]);
      })
      .catch((err) => console.log('GET_CART_INFO', err));
  },

};
