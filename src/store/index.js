/*
GLOBAL VUEX
*/
import Vue from 'vue';
import Vuex from 'vuex';
//import other vuex modules
import Booking from './modules/Booking';
import CheckIn from './modules/CheckIn';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  state: {
    loading: false,
  },

  mutations: {
    loading(state, status) {
      state.loading = status;
    },
  },

  modules: {
    //Store Modules
    Booking,
    CheckIn,
  },
});
