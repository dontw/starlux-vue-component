import Vue from 'vue';
import VeeValidate, { Validator } from "vee-validate";
import i18n from "../i18n";
import zh_TW from "vee-validate/dist/locale/zh_TW";
import en from "vee-validate/dist/locale/en";
import ja from "vee-validate/dist/locale/ja";

Vue.use(VeeValidate, {
  i18n,
  // i18nRootKey: 'validation',
  dictionary: {
    'zh-TW': zh_TW,
    'en': en,
    'ja': ja,
  },
});

// const Dictionary = {
//   'zh-TW': {
//     messages: {
//       required: '這個是必填的',
//       email: field => field + '格式不正確',
//     },
//   },
//   'ja': {
//     messages: {
//       required: 'ja',
//       email: field => field + 'ja',
//     },
//   },
//   'en': {
//     messages: {
//       required: 'en',
//       email: field => field + 'en',
//     },
//   },
// };

// Validator.localize(Dictionary);
