import Vue from 'vue';
import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import { checkA11y } from '@storybook/addon-a11y';
import { withReadme } from 'storybook-readme';
import Centered from '@storybook/addon-centered';
import README from './README.md';

import StarDropdown from './';

Vue.component('StarDropdown', StarDropdown);

storiesOf('StarDropdown', module)
  .addDecorator(Centered)
  .addDecorator(checkA11y)
  .add(
    'Default',
    withReadme([README], () => ({
      template: `
              <div style="width: 200px;">
                <StarDropdown
                  title="欄位名稱"
                  placeholder="請選擇"
                  elementId="test"
                  v-model="testData"
                  :options="options"
                  :disabled="false"
                />
              </div>`,
      data() {
        return {
          options: [
            {label: '項目名稱1', value: 1},
            {label: '項目名稱2', value: 2},
            {label: '項目名稱3', value: 3},
            {label: '項目名稱4', value: 4},
            {label: '項目名稱5', value: 5},
            {label: '項目名稱6', value: 6},
            {label: '項目名稱7', value: 7},
            {label: '項目名稱8', value: 8},
            {label: '項目名稱9', value: 9},
            {label: '項目名稱10', value: 10},
            {label: '項目名稱11', value: 11},
            {label: '項目名稱12', value: 12},
          ],
          testData: null,
        };
      },
      methods: {
        log(val) {
          action('StarDropdown')(val);
        },
      },
      watch: {
        testData(val) {
          this.log(val);
        }
      }
    })),
    { jest: ['StarDropdown.test.js'] },
  )
  .add(
    'Disabled',
    withReadme([README], () => ({
      template: `
              <div style="width: 200px;">
                <StarDropdown
                  title="欄位名稱"
                  placeholder="請選擇"
                  elementId="test"
                  v-model="testData"
                  :options="options"
                  :disabled="true"
                />
              </div>`,
      data() {
        return {
          options: [
            {label: '項目名稱1', value: 1},
            {label: '項目名稱2', value: 2},
            {label: '項目名稱3', value: 3},
            {label: '項目名稱4', value: 4},
            {label: '項目名稱5', value: 5},
            {label: '項目名稱6', value: 6},
            {label: '項目名稱7', value: 7},
            {label: '項目名稱8', value: 8},
            {label: '項目名稱9', value: 9},
            {label: '項目名稱10', value: 10},
            {label: '項目名稱11', value: 11},
            {label: '項目名稱12', value: 12},
          ],
          testData: null,
        };
      },
      methods: {
        log(val) {
          action('StarDropdown')(val);
        },
      },
      watch: {
        testData(val) {
          this.log(val);
        }
      }
    })),
    { jest: ['StarDropdown.test.js'] },
  );

