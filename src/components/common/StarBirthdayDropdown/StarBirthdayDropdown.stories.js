import Vue from 'vue';
import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import { checkA11y } from '@storybook/addon-a11y';
import { withReadme } from 'storybook-readme';
import Centered from '@storybook/addon-centered';
import README from './README.md';

import StarBirthdayDropdown from './';

Vue.component('StarBirthdayDropdown', StarBirthdayDropdown);

storiesOf('StarBirthdayDropdown', module)
  .addDecorator(Centered)
  .addDecorator(checkA11y)
  .add(
    'Default',
    withReadme([README], () => ({
      template: `<div><StarBirthdayDropdown name="birthday" v-model="test"/>{{test}}</div>`,
      data() {
        return {
          test: null,
        };
      },
      methods: {
        log(val) {
          action('StarBirthdayDropdown')(val);
        },
      },
    })),
    { jest: ['StarBirthdayDropdown.test.js'] },
  );
