import Vue from 'vue';
import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import { checkA11y } from '@storybook/addon-a11y';
import { withReadme } from 'storybook-readme';
import Centered from '@storybook/addon-centered';
import README from './README.md';
import StarPaxStepper from './';

Vue.component('StarPaxStepper', StarPaxStepper);

storiesOf('StarPaxStepper', module)
  .addDecorator(Centered)
  .addDecorator(checkA11y)
  .add(
    'Default',
    withReadme([README], () => ({
      template: `<StarPaxStepper
                class="border border-ivory-dark p-3"
                v-model="ADT"
                name="adult"
                :max="9"
                :min="0"
                :maxTotal="ADT">
                <template slot="title">
                <div class="d-flex flex-column align-items-start justify-content-center">
                  <span>成人</span>
                  <small class="d-block">十二歲以上</small>
                </div>
              </template>
                </StarPaxStepper>`,
      data() {
        return {
          ADT: 0,
        };
      },
      methods: {
        log(val) {
          action('StarPaxStepper')(val);
        },
      },

      watch: {
        ADT() {
          this.log(this.ADT);
        },
      },
    })),
    { jest: ['StarPaxStepper.test.js'] },
  );
