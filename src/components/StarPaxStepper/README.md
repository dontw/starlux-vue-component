# StarPaxStepper 計步器

用於旅客數量輸入

| name       | description      | type   | default | sample\* |  必須  |
| ----------- | ---------------- | ------ | ------- | -------- | -------- |
| value       | 數量           | Number | 無      | 1         |
| max         | 最大值限制      | Number | 無      | 9        | ✅        |
| min         | 最小值限制      | Number | 無      | 1     | ✅        |
| maxTotal    | 目前總旅客數量 | Number | 無 | 9| |
