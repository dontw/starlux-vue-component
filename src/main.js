import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/';
import i18n from './i18n';
import './validator';
import filters from './plugin/filters';
import methods from './plugin/methods';

//vendor modules
import VTooltip from 'v-tooltip';
import VueClipboards from 'vue-clipboards';
import Notifications from 'vue-notification';
import velocity from 'velocity-animate';
import { cloneDeep } from 'lodash-es';
import VueSkipTo from 'vue-skip-to';
import VueAnnouncer from 'vue-announcer';

//main styles
import '@/style/main.scss';

// APIs
Vue.config.productionTip = false;
Vue.use(VueClipboards);
Vue.use(filters);
Vue.use(methods);
Vue.use(VTooltip);
Vue.use(VueAnnouncer);
Vue.use(VueSkipTo);
Vue.use(Notifications, { velocity });

const lodashFunctions = {
  cloneDeep,
};

Object.defineProperty(Vue.prototype, '$_', { value: lodashFunctions });

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount('#app');
