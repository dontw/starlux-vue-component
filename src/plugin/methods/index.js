export default {
  install(Vue) {
    Vue.mixin({
      methods: {
        formatDate(value) {
          if (value) {
            return `${this.$d(new Date(value), 'long')}, ${this.$d(
              new Date(value),
              'weekday',
            )}`;
          }
        },

        /**
         * 轉大寫
         * @param {String} value
         * @returns {String}
         */
        upperCase(value) {
          if (value) {
            return value.toUpperCase();
          }
        },
      },
    });
  },
};
