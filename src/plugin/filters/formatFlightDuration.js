export default function formatFlightDuration(value) {
  if (value) {
    let hour = Math.floor(value / 60);
    let min = value - hour * 60;
    return `${hour} 小時 ${min} 分`;
  }
}
