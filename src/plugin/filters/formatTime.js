export default function formatTime(value) {
  if (value) {
    return value
      .split(':')
      .splice(0, 2)
      .join(':');
  }
}
