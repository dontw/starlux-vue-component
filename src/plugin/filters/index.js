import formatTime from './formatTime';
import formatFlightDuration from './formatFlightDuration';

export default {
  install(Vue) {
    Vue.filter('formatTime', formatTime);
    Vue.filter('formatFlightDuration', formatFlightDuration);
  },
};
