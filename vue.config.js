module.exports = {
  css: {
    loaderOptions: {
      sass: {
        data: `
        @import "@/style/_functions.scss";
        @import "@/style/_variables.scss";
        @import "@/style/_mixins.scss";
        `,
      },
    },
  },
  devServer: {
    host: '0.0.0.0',
    port: 1288,
    disableHostCheck: true,
    proxy: 'https://api.starlux-airlines.com/api',
  },
};
